jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
}));

import DetailsTable, { getData, Post } from "./components/DetailsTable";
import renderer from "react-test-renderer";
import { render, waitFor, act } from "@testing-library/react";
import axios from "axios";
import { BrowserRouter } from "react-router-dom";

test("Details Table snapshot", () => {
  const component = renderer.create(
    <BrowserRouter>
      <DetailsTable />
    </BrowserRouter>
  );
  expect(component).toMatchSnapshot();
});

test("table getting rendered", async () => {
  const { getByTestId } = render(<DetailsTable />);
  expect(getByTestId("tablebody")).toBeInTheDocument();
});

test("check if data being fetched from axios", () => {
  act(async () => {
    const data = await getData(1);
    () => expect(JSON.stringify(data)).toHaveLength(20);
  });
});
