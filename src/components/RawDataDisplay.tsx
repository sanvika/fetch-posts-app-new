import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { useLocation, useNavigate } from "react-router-dom";
import Button from "@mui/material/Button";
import { Post } from "./DetailsTable";

const RawDataDisplay = () => {
  const navigate = useNavigate();
  const { state } = useLocation();
  const post = state as Post;

  const handleClick = () => {
    navigate("/");
  };
  return (
    <Grid container direction="column" rowSpacing={2} style={{ overflowX: "hidden" }}>
      <Grid item key={post.objectID}>
        <Typography data-testid="dataDisplay" variant="h6">
          {JSON.stringify(post)}
        </Typography>
      </Grid>
      <Grid item style={{ paddingLeft: "50%" }}>
        <Button variant="contained" onClick={handleClick}>
          Back
        </Button>
      </Grid>
    </Grid>
  );

  //   return <Grid container>{database.filter((post) => post.objectID == objectId)}</Grid>;
};

export default RawDataDisplay;
