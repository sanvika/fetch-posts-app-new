import { useState, useEffect, useRef, useContext } from "react";
import axios from "axios";
import { AxiosError } from "axios";
import { Link as RouterLink } from "react-router-dom";
import Link from "@mui/material/Link";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { PostsContext } from "../App";

export type Post = {
  title: string;
  url: string;
  author: string;
  created_at: string;
  objectID: string;
};

const tableHeader = {
  title: "Title",
  author: "Author",
  created_at: "Created At",
  url: "url",
};
const formatDateTime = (date: string) => {
  const newDate = new Date(date);
  return newDate.toDateString() + " " + newDate.toLocaleTimeString();
};

export const getData = async (pageNumber: number): Promise<Post[] | Error> => {
  try {
    const response = await axios.get("https://hn.algolia.com/api/v1/search_by_date?tags=story&page=" + pageNumber);
    const data = response.data.hits as Post[];
    return data;
  } catch (err) {
    const error = err as AxiosError;
    console.error(error);
    return new Error(error.message);
  }
};
function DetailsTable() {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<Error>();
  const [pageNumber, setPageNumber] = useState(0);
  const interval = useRef<NodeJS.Timer>();

  const { postContext, setPostContext } = useContext(PostsContext);
  //changes

  //
  useEffect(() => {
    setLoading(false);
    (async () => {
      const newPosts = await getData(pageNumber);
      if (newPosts instanceof Error) {
        setError(newPosts);
      } else {
        setPostContext((postContext) => {
          const newPostContext = [...postContext, ...newPosts];
          return newPostContext;
        });
      }
    })();
  }, [pageNumber]);

  useEffect(() => {
    interval.current = setInterval(() => {
      setPageNumber((pageNumber) => pageNumber + 1);
    }, 10_000);
    return () => {
      if (interval.current) clearInterval(interval.current);
    };
  }, []);

  return (
    <>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow style={styles.tableHeader}>
              <TableCell style={{ border: "2px solid black" }}>{tableHeader.title}</TableCell>
              <TableCell style={{ border: "2px solid black" }}>{tableHeader.author}</TableCell>
              <TableCell style={{ border: "2px solid black" }}>{tableHeader.created_at}</TableCell>
              <TableCell style={{ border: "2px solid black" }}>{tableHeader.url}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody data-testid="tablebody">
            {postContext.map((item: Post, index) => (
              <TableRow key={index} style={styles.tableBody}>
                <TableCell>
                  <Link component={RouterLink} to={`/detail`} state={item}>
                    {item.title}
                  </Link>
                </TableCell>
                <TableCell>
                  <Link component={RouterLink} to={`/detail`} state={item}>
                    {item.author}
                  </Link>
                </TableCell>
                <TableCell>
                  <Link component={RouterLink} to={`/detail`} state={item}>
                    {formatDateTime(item.created_at)}
                  </Link>
                </TableCell>
                <TableCell>
                  <Link component={RouterLink} to={`/detail`} state={item}>
                    {item.url}
                  </Link>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <label>{loading && "Loading..."}</label>
    </>
  );
}
const styles = {
  labels: {
    color: "#2f2fa2",
  },
  tableHeader: {
    background: "#84CDCA",
  },
  tableBody: {
    background: "#AFE0DE",
  },
};
export default DetailsTable;
