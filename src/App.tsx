import { createContext, useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import DetailsTable from "./components/DetailsTable";
import RawDataDisplay from "./components/RawDataDisplay";

export const PostsContext = createContext<{
  postContext: any[];
  setPostContext: React.Dispatch<React.SetStateAction<any[]>>;
}>({
  postContext: [],
  setPostContext: () => {},
});

function App() {
  const [postContext, setPostContext] = useState<any[]>([]);
  return (
    <PostsContext.Provider value={{ postContext, setPostContext }}>
      <BrowserRouter>
        <Routes>
          <Route index element={<DetailsTable />} />
          <Route path="/detail" element={<RawDataDisplay />} />
        </Routes>
      </BrowserRouter>
    </PostsContext.Provider>
  );
}

export default App;
