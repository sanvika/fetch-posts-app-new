jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({ state: { title: "string", url: "string", author: "string", created_at: "string", objectID: "string" } }),
}));

import RawDataDisplay from "./components/RawDataDisplay";
import renderer from "react-test-renderer";
import { fireEvent, render, waitFor } from "@testing-library/react";
import axios from "axios";
import { BrowserRouter } from "react-router-dom";
import { Button } from "@mui/material";
//data-testid

test("renders App.match snapshot", () => {
  const component = renderer.create(
    <BrowserRouter>
      <RawDataDisplay />
    </BrowserRouter>
  );
  expect(component).toMatchSnapshot();
});

test("renders the data on the screen", () => {
  const { getByTestId } = render(
    <BrowserRouter>
      <RawDataDisplay />
    </BrowserRouter>
  );
  expect(getByTestId("dataDisplay")).toBeInTheDocument();
});

test("button to be tested and clicked", () => {
  const { getByRole } = render(
    <BrowserRouter>
      <RawDataDisplay />
    </BrowserRouter>
  );
  expect(getByRole("button")).toBeInTheDocument();
  fireEvent.click(getByRole("button"));
});
