const mockedUsedNavigate = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
}));
import App from "./App";
import renderer from "react-test-renderer";

test("renders App.match snapshot", () => {
  const component = renderer.create(<App />);
  expect(component).toMatchSnapshot();
});
